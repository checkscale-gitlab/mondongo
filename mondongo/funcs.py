#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

import tqdm

PRICE = float(os.getenv("SPOYDERMAN_PRICE", 0.012))


def listDB(client):
    """ Lista todas las bases de datos relacionadas a proyecto spoyderman
    """
    dbs = client.database_names()
    for db in dbs:
        if "spoyderman_" in db:
            print(db.replace("spoyderman_", "📕 "))


def countDB(client, db, beg, end):
    """Cuenta los documentos en la base de datos

    db: nombre de la base de datos de interes
    beg: inicio de la colección
    end: cuanto recoger, -1 equivale a todo lo que hay en la base de datos

    Por motivos de conveniencia retorna una cadena formateada con el precio de
    la acotación de records, la cantidad de records y la misma colección

    """
    tdb = client["spoyderman_" + db]

    if beg < 0:
        beg = 0

    if end <= 0:
        collections = tdb.records.find()[beg:]
    else:
        collections = tdb.records.find()[beg:end]

    count = collections.count()
    return ("\n💸 USD$ {:.3f} ({} records)".format(count * PRICE, count),
            count, collections)


def exportDB(client, db, exporter, beg=0, end=0):
    """ Exporta los documentos a un formato especificado por `exporter`

    db: nombre de la base de datos de interes
    exporter: una clase que se encarga del formateo
    beg: inicio de la colección
    end: cuanto recoger, `all` equivale a todo lo que hay en la base de datos
    """
    money, count, collections = countDB(client, db, beg, end)

    # Nombra el archivo al cual se hará la exportación de records
    exporter.filename(db + "_{}records_.".format(count) +
                      exporter.extension().lower())

    for collection in tqdm.tqdm(
            collections,
            total=count,
            desc="to {}".format(exporter.extension())):
        exporter.insert(collection)

    exporter.close()
    print(money)
