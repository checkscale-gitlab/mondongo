#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import os
import sys

import pymongo

from mondongo import exporters, funcs

MONGO_URI = os.getenv("MONGO_URI", "mongodb://localhost:27017/")
MC = pymongo.MongoClient(MONGO_URI)
DESC = "Exporta documentos de una colección de MongoDB"
EXPDESC = ("Exporta la base de datos con ciertos limites"
           " indicados por `begin` y `end`. El archivo sera creado"
           " en el directorio `directory`.")

# Registra los exportadores validos
vexporters = {"csv": exporters.CSVExporter,
              "tsv": exporters.TSVExporter,
              "json": exporters.JSONExporter}

# Crea las banderas para la linea de comandos
banderas = argparse.ArgumentParser(description=DESC)
banderas.add_argument(
    "--list", help="Lista todas las bases de datos disponibles",
    action="store_true")
ops = banderas.add_mutually_exclusive_group()
ops.add_argument("--count", metavar=("base de datos"),
                 help="Retorna el numero total de documentos")
ops.add_argument("--export", nargs=5,
                 metavar=("/".join(vexporters.keys()), "database",
                          "begin", "end", "directory"),
                 help=EXPDESC)


def main(argv=None):
    ARGS = banderas.parse_args(argv)

    if ARGS.count:
        # El usuario desea conocer la cantidad de records en la base de datos
        # `ARGS.count` contiene el nombre de la base de datos
        r = funcs.countDB(MC, ARGS.count, 0, 0)
        print(r[0])
    elif ARGS.list:
        funcs.listDB(MC)
    elif ARGS.export:
        extension, db, beg, end, output = ARGS.export
        if extension.lower() not in vexporters.keys():
            sys.exit("!!: {} no es valido como formato de salida, escoja {}".format(
                extension, vexporters.keys()))

        exp = vexporters[extension.lower()](output)
        try:
            funcs.exportDB(MC, db, exp, int(beg), int(end))
        except TypeError:
            sys.exit("!!: Existe la base de datos spoyderman_%s?" % db)
