#!/usr/bin/env python
# -*- coding: utf-8 -*-

import tempfile

import nose
from scrapy.exceptions import DropItem

from mondongo import flags, pipeline


class DummySpider():
    name = "test"


dummyspider = DummySpider()


def test_noargs():
    flags.main([])


def test_count():
    argv = "--count test".split(" ")
    flags.main(argv)


def test_list():
    argv = "--list".split(" ")
    flags.main(argv)


def test_export_csv():
    # crea un directorio temporal
    with tempfile.TemporaryDirectory(__name__) as tdir:
        argv = "--export csv test 0 0 {}".format(tdir).split(" ")
        flags.main(argv)


def test_export_tsv():
    # crea un directorio temporal
    with tempfile.TemporaryDirectory(__name__) as tdir:
        argv = "--export tsv test 0 0 {}".format(tdir).split(" ")
        flags.main(argv)


def test_export_json():
    # crea un directorio temporal
    with tempfile.TemporaryDirectory(__name__) as tdir:
        argv = "--export json test 0 0 {}".format(tdir).split(" ")
        flags.main(argv)


@nose.tools.raises(SystemExit)
def test_export_fail():
    # crea un directorio temporal
    with tempfile.TemporaryDirectory(__name__) as tdir:
        argv = "--export fail test 0 0 {}".format(tdir).split(" ")
        flags.main(argv)


def test_pipeline():
    item = {"name": "Jack Sparrow"}
    pipe = pipeline.SpoyderPipeline()
    pipe.open_spider(dummyspider)
    r = pipe.process_item(item, dummyspider)
    pipe.close_spider(dummyspider)
    nose.tools.eq_(r, item, "Resultado inesperado")


@nose.tools.raises(DropItem)
def test_pipeline_fail():
    item = {"name": "Jack Sparrow"}
    pipe = pipeline.SpoyderPipeline()
    pipe.open_spider(dummyspider)
    pipe.process_item(item, dummyspider)
    pipe.process_item(item, dummyspider)  # debe fallar aquí
