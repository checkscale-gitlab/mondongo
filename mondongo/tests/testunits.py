#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest

import mongomock

from mondongo import exporters, funcs

RECORDS = [
    {"name": "John Wick", "City": "New York", "Phone": "315-077-2231"},
    {"name": "Jason Bourne", "City": "New York", "Phone": "212-664-7665"},
    {"name": "Goku", "City": "Montaña Paoz", "Phone": "0-000-000"}
]


class Test(unittest.TestCase):
    def setUp(self):
        self.mc = mongomock.MongoClient()
        self.mc.spoyderman_test.records.insert_many(RECORDS)

    def test_listdb(self):
        funcs.listDB(self.mc)

    def test_countdb(self):
        funcs.countDB(self.mc, "test", -1, -1)

    def test_countdb_limit(self):
        funcs.countDB(self.mc, "test", 0, 1)

    def test_exportdb(self):
        funcs.exportDB(self.mc, "test", exporters.DummyExporter(""), 0, 0)
