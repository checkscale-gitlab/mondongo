#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

from setuptools import setup
from setuptools.command.test import test as TestCommand


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

# Inspired by the example at https://pytest.org/latest/goodpractises.html


class NoseTestCommand(TestCommand):
    def finalize_options(self):
        TestCommand.finalize_options(self)
        self.test_args = []
        self.test_suite = True

    def run_tests(self):
        # Run nose ensuring that argv simulates running nosetests directly
        import nose
        nose.run_exit(argv=['nosetests'])


package = 'mondongo'
version = '0.1'

setup(name=package,
      version=version,
      author="Jorge Javier Araya Navarro",
      author_email="jorge@esavara.cr",
      description="Scrapy y MongoDB",
      long_description=read('README.md'),
      license="MIT",
      cmdclass={'test': NoseTestCommand},
      tests_require=["nose", "mongomock", "coverage"],
      packages=["mondongo"],
      scripts=['bin/mondongo'],
      url='https://gitlab.com/spoyderman/mondongo',
      install_requires=[
          'pymongo>=3.5.1',
          'tqdm>=4.19.4',
          'Scrapy>=1.4.0',
      ])
