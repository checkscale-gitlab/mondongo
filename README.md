# Mondongo: Scrapy y MongoDB🕷 [![pipeline status](https://gitlab.com/spoyderman/mondongo/badges/master/pipeline.svg)](https://gitlab.com/spoyderman/mondongo/commits/master) [![coverage report](https://gitlab.com/spoyderman/mondongo/badges/master/coverage.svg)](https://gitlab.com/spoyderman/mondongo/commits/master)

## Expectativas

La herramienta espera que las bases de datos relacionados a la minería hecha para clientes del proyecto Spoyderman comiencen en su nombre con `spoyderman_` y que los récords guardados esten en la colección `records`. Este requerimiento estara satisfecho si se usa el pipeline `SpoyderPipeline` en la configuración del proyecto Scrapy.

## Instalación

```
pip install git+https://gitlab.com/spoyderman/mondongo.git
```

## Uso

### herramienta de exportación

Utilizar la vieja confiable `--help` para ver detalles de uso.

```
mondongo --help
```

### Tubería de ítem

Luego de creado el proyecto Scrapy con `scrapy startproject prueba` se edita el archivo `prueba/prueba/settings.py` y se coloca la siguiente sentencia.

```
ITEM_PIPELINES = {
    # ...
    'mondongo.pipeline.SpoyderPipeline': 300,
    # ...
}
```

Todos los ítem minados serán guardados en `spoyderman_<nombre de la araña>.records` dentro de MongoDB. Ningún ítem repetido será admitido.
